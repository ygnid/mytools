#!/usr/bin/env python2.7

# split_merra.py

import os, sys
#import re
#from datetime import datetime, timedelta
#from dateutil.parser import parse
import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
#from mpl_toolkits.basemap import Basemap
#from matplotlib import mlab
#import pandas as pd
from netCDF4 import Dataset
from netCDF4 import num2date as n2d
from netCDF4 import date2num as d2n
from metlib.kits import *
from metlib.shell import LS_R
from metlib.phy.wind import uv2wswd
#from handle_files import *
#from parts import *


varname_list = ['Times', 'XLAT', 'XLONG', 'XLAT_U', 'XLONG_U', 'XLAT_V', 'XLONG_V', 'U','V','W','QCLOUD']


def extract_nc_var(in_ncf, out_ncf, varname, time_index):
    in_var = in_ncf.variables[varname]
    out_var = out_ncf.createVariable(varname, in_var.dtype, in_var.dimensions)
    out_var[:] = in_var[time_index,:]
    copy_nc_var_attrs(in_var, out_var)


def copy_nc_var_attrs(in_var, out_var):
    #out_var._FillValue = in_var._FillValue
    for attr in in_var.ncattrs():
        if attr != '_FillValue':
            out_var.setncattr(attr, in_var.getncattr(attr))


if __name__ == '__main__':
    input_ncfile = 'wrfout_d03_2016-09-12_12:00:00'
    output_ncfile = 'split_d03.nc'
    in_ncf = Dataset(input_ncfile, 'r')
    out_ncf = Dataset(output_ncfile, 'w')

    # copy dimensions
    for dim, desc in in_ncf.dimensions.items():
        if desc.isunlimited():
            out_ncf.createDimension(dim, size=None)
        else:
            out_ncf.createDimension(dim, size=desc.size)

    # copy vars, resample time from 15min to 1hour
    time_num = in_ncf.dimensions['Time'].size
    time_index = np.arange(0, time_num, 4, dtype='int32')
    for varname in varname_list:
        print 'extracting %s...' % varname
        extract_nc_var(in_ncf, out_ncf, varname, time_index)

    in_ncf.close()
    out_ncf.close()


