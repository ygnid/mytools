#!/usr/bin/env python
#coding:utf-8
__author__ = 'ding'

import sys
from os import path
from pykml.factory import KML_ElementMaker as KML
from lxml import etree


def save_kml(kml, filename):
    fid = file(filename, 'w')
    fid.write(etree.tostring(kml, pretty_print=True))


def get_params():
    if len(sys.argv) == 2:
        input_file = sys.argv[1]
        kml_file = path.splitext(path.basename(input_file))[0] + ".kml"
        doc_name = None
        return input_file, kml_file, doc_name
    elif len(sys.argv) == 3:
        input_file = sys.argv[1]
        kml_file = path.splitext(path.basename(input_file))[0] + ".kml"
        doc_name = sys.argv[2].decode("utf-8")
        return input_file, kml_file, doc_name
    elif len(sys.argv) == 4:
        input_file = sys.argv[1]
        kml_file = sys.argv[2]
        doc_name = sys.argv[3].decode("utf-8")
        return input_file, kml_file, doc_name
    else:
        print "Please input params, Usage:"
        print "    python %s input_file [kml_file] [doc_name]" % sys.argv[0]
        print "or: python %s input_file [doc_name]" % sys.argv[0]
        sys.exit(1)


def convert_stnfile_to_kml(stn_file, kml_file, doc_name=None):

    doc = KML.Document()
    if doc_name:
        doc.append(KML.name(doc_name))
    with open(stn_file, 'r') as f:
        for line in f:
            stnid, lat, lon = line.strip().split(",")
            stnid = stnid.decode("utf-8")
            print(stnid, lat, lon)
            pm = KML.Placemark(
                KML.name(stnid),
                KML.Point(
                    KML.coordinates("%s,%s" % (lon, lat))
                )
            )
            doc.append(pm)

    save_kml(doc, kml_file)


if __name__ == "__main__":
    stn_file, kml_file, doc_name = get_params()
    convert_stnfile_to_kml(stn_file, kml_file, doc_name=doc_name)
