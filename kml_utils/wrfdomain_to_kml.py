#!/usr/bin/env python
# coding:utf-8
__author__ = 'ding'

import sys

libpath = "/home/dingyu/python_lib/"
sys.path.append(libpath)

from pykml.factory import KML_ElementMaker as KML
from lxml import etree
from metlib.wrf.wps import *

level_styles = {
    1: {"line_color": "99c7ff1f", "line_width": 2, "poly_color": "80c8ff55", "fill": 0},
    2: {"line_color": "99ff9348", "line_width": 2, "poly_color": "99ff9348", "fill": 1},
    3: {"line_color": "9966ffcc", "line_width": 2, "poly_color": "8066ffcc", "fill": 1},
    4: {"line_color": "99ffcc66", "line_width": 2, "poly_color": "80ffcc66", "fill": 1},
}


def get_namelist_domain_edges(namelist):
    domainer = Domainer(namelist)
    domains = domainer.domains
    domain_edges = dict()
    level_domains = domainer.level_domains
    max_dom = domainer.max_dom
    print max_dom

    for domain in domainer.domains:
        g = domainer.cross_grids[domain]
        domain_edge = g.edge
        domain_edges[domain] = domain_edge
    return domains, level_domains, domain_edges


def make_style(style_name="default_style", scale=1.1, line_color="ffc7ff1f", poly_color="80c8ff55", fill=0):
    style = KML.Style(
        KML.IconStyle(
            KML.scale(scale),
            KML.Icon(
                KML.href("http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png")
            ),
            KML.hotSpot(
                x="20", y="2", xunits="pixels", yunits="pixels"
            ),
        ),
        KML.LineStyle(
            KML.color(line_color)
        ),
        KML.PolyStyle(
            KML.color(poly_color),
            KML.fill(fill)
        ),
        id=style_name
    )
    return style


def make_style_map(style_map_name, style1_name, style2_name):
    assert isinstance(style_map_name, str)
    assert isinstance(style1_name, str)
    assert isinstance(style2_name, str)
    style_map = KML.StyleMap(
        KML.Pair(
            KML.key("normal"),
            KML.styleUrl("#%s" % style1_name)
        ),
        KML.Pair(
            KML.key("highlight"),
            KML.styleUrl("#%s" % style2_name)
        ),
        id=style_map_name
    )
    return style_map


def make_polygon(domain_edge, name="random domain", style="default_style"):
    lon, lat = domain_edge
    coord_list = [ "%f,%f,0" % (x, y) for x, y in zip(lon,lat) ]
    domain_coordinates = " ".join(coord_list)
    polygon = KML.Placemark(
        KML.name(name),
        KML.styleUrl("#%s" % style),
        KML.Polygon(
            KML.tessellate(1),
            KML.outerBoundaryIs(
                KML.LinearRing(
                    KML.coordinates(domain_coordinates)
                )
            )
        )
    )
    return polygon


def make_doc(parts):
    doc = KML.Document()
    for part in parts:
        doc.append(part)
    return doc


def save_kml(kml, filename):
    fid = file(filename, 'w')
    fid.write(etree.tostring(kml, pretty_print=True))


def convert_namelist_to_kml(input_namelist, output_kml_file, level_style_list=level_styles, doc_name=None):
    domains, level_domains, domain_edges = get_namelist_domain_edges(input_namelist)
    style_list = []
    polygon_list = []
    for level in level_domains:
        level_id = "level%d" % level
        style = make_style(style_name=level_id,
                           line_color=level_style_list[level]["line_color"],
                           poly_color=level_style_list[level]["poly_color"],
                           fill=level_style_list[level]["fill"])
        for domain in level_domains[level]:
            domain_id = "domain%d" % domain
            print level_style_list[level]
            polygon = make_polygon(domain_edges[domain], name=domain_id, style=level_id)
            style_list.append(style)
            polygon_list.append(polygon)

    if doc_name:
        print doc_name
        print type(doc_name)
        kml_name = KML.name(doc_name)
        all_list = [kml_name] + style_list + polygon_list
    else:
        all_list = style_list + polygon_list
    kml = make_doc(all_list)
    save_kml(kml, output_kml_file)


def get_params():
    if len(sys.argv) == 3:
        namelist = sys.argv[1]
        doc_name = sys.argv[2].decode("utf-8")
        kml_file = doc_name + ".kml"
        return namelist, kml_file, doc_name
    else:
        print "Please input params,Usage:"
        print "  python %s namelist_file kml_name(without .kml extension)" % sys.argv[0]
        sys.exit(1)


if __name__ == '__main__':
    params = get_params()
    namelist, kml_file, doc_name = params
    convert_namelist_to_kml(namelist, kml_file, doc_name=doc_name)
