#!/usr/bin/env python
# coding: utf-8

__author__ = 'ding'

import pandas as pd
import numpy as np
import matplotlib as mpl
from matplotlib.patches import FancyBboxPatch
from matplotlib.projections.polar import PolarAxes
import matplotlib.pyplot as plt
from pylab import poly_between
from windrose import WindroseAxes


RESOLUTION = 100
ZBASE = -1000  # The starting zorder for all drawing, negative to have the grid on
VAR_DEFAULT = 'speed'
DIR_DEFAULT = 'direction'
FIGSIZE_DEFAULT = (8, 8)
DPI_DEFAULT = 80


class MyWindroseAxes(WindroseAxes):

    @classmethod
    def from_ax(cls, ax=None, fig=None, *args, **kwargs):
        if ax is None:
            if fig is None:
                fig = plt.figure(figsize=FIGSIZE_DEFAULT,
                                 dpi=DPI_DEFAULT, facecolor='w', edgecolor='w')
            rect = [0.1, 0.1, 0.8, 0.8]
            ax = cls(fig, rect, axisbg='w', *args, **kwargs)
            fig.add_axes(ax)
            return fig, ax
        else:
            return fig, ax

    def _init_plot(self, direction, var, **kwargs):
        """
        Internal method used by all plotting commands
        """
        wind_rose = direction
        wind_sector_dist = var
        # self.cla()
        kwargs.pop('zorder', None)

        # Init of the bins array if not set
        bins = kwargs.pop('bins', None)
        if bins is None:
            bins = wind_sector_dist.index.insert(0, 0.)
            #bins = wind_sector_dist.index
        bins = np.asarray(bins)
        nbins = len(bins) - 1

        # Number of sectors
        nsector = kwargs.pop('nsector', None)
        if nsector is None:
            nsector = len(wind_rose)

        # Sets the colors table based on the colormap or the "colors" argument
        colors = kwargs.pop('colors', None)
        cmap = kwargs.pop('cmap', None)
        if colors is not None:
            if isinstance(colors, str):
                colors = [colors] * nbins
            if isinstance(colors, (tuple, list)):
                if len(colors) != nbins:
                    raise ValueError("colors and bins must have same length")
        else:
            if cmap is None:
                cmap = mpl.cm.jet
            colors = self._colors(cmap, nbins)

        # Building the angles list
        angles = np.arange(0, -2 * np.pi, -2 * np.pi / nsector) + np.pi / 2

        normed = kwargs.pop('normed', True)
        blowto = kwargs.pop('blowto', False)

        # Set the global information dictionnary
        angle = 360. / nsector
        dir_bins = np.arange(-angle / 2, 360. + angle, angle, dtype=np.float)
        dir_edges = dir_bins.tolist()
        dir_edges.pop(-1)
        dir_edges[0] = dir_edges.pop(-1)
        dir_bins[0] = 0.

        var_bins = bins.tolist()
        table = np.asarray(wind_rose) * np.asarray(wind_sector_dist)
        self._info['dir'], self._info['bins'], self._info[
            'table'] = (dir_edges, var_bins, table)

        return bins, nbins, nsector, colors, angles, kwargs


class MetTABReader(object):
    site_name = None
    longitude = None
    latitude = None
    height = None
    num_sector = None
    wspd_interval = None
    wdir_offset = None
    wind_rose = None
    wind_sector_dist = None
    reformed_wind_sector_dist = None

    def __init__(self, tabfile):
        self.tabfile = tabfile
        with open(self.tabfile, 'r') as f:
            line = f.readline()
            self.site_name = line.split('|')[0].strip()
            self.latitude, self.longitude, self.height = map(
                float, f.readline().strip().split())
            self.num_sector, self.wspd_interval, self.wdir_offset = map(
                float, f.readline().strip().split())
            self.num_sector = int(self.num_sector)
            self.wdir_interval = 360.0 / self.num_sector
            self.wdir_sectors = np.arange(
                self.wdir_offset, self.wdir_offset + 360, self.wdir_interval)
            self.wind_rose = pd.Series(
                map(float, f.readline().strip().split()))
            self.wind_rose.index = self.wdir_sectors
        self.wind_sector_dist = pd.read_table(
            self.tabfile, header=None, skiprows=4, sep='\s+', index_col=0)
        self.wind_sector_dist.columns = self.wdir_sectors

    def print_data(self):
        if self.wind_rose is None:
            raise ValueError('MetTABReader not initialized, nothing to print.')
        print 'site name: %s, lat: %s, lon: %s, height: %s' % (self.site_name, self.latitude, self.longitude, self.height)
        print 'wind direction sectors:'
        print self.wdir_sectors
        print 'wind rose:\n', self.wind_rose
        print 'wind distribution by sectors:\n', self.wind_sector_dist

    def plot_bar(self, png='output.png'):
        fig, axes = MyWindroseAxes.from_ax()
        wind_rose = self.wind_rose / 100.
        if self.reformed_wind_sector_dist is None:
            wind_sector = self.wind_sector_dist / 1000.
        else:
            wind_sector = self.reformed_wind_sector_dist / 1000.

        axes.bar(wind_rose, wind_sector, normed=True,
                 opening=0.8, edgecolor='white')
        axes.set_legend()
        if png:
            fig.patch.set_alpha(0.5)
            axes.patch.set_alpha(0.5)
            fig.savefig(png, format='png')
        return fig, axes

    def strip_bins(self):
        '''
        find the largest wspd bin with non-zero value, then limit bins to this one.
        '''
        ibin = len(self.wind_sector_dist.index) - 1
        is_nonzero = False
        while not is_nonzero:
            sum_bin = sum(self.wind_sector_dist.iloc[ibin, :])
            if sum_bin > 0.0:
                is_nonzero = True
            else:
                ibin -= 1
        self.wind_sector_dist = self.wind_sector_dist[0:ibin]

    def reform_sector_dist(self, interval=5):
        if self.wspd_interval >= interval:
            raise ValueError(
                'reformed windspeed interval must be larger than the original interval.')
        # first strip empty bins
        self.strip_bins()
        # 2. figure out how many bins in new distribution
        orig_bins = self.wind_sector_dist.index
        orig_nbins = len(orig_bins)
        new_nbins = int(np.ceil(orig_bins[-1] / interval))
        new_bins = np.arange(0, new_nbins * interval, interval) + interval
        new_dist = np.zeros([new_nbins, self.num_sector])
        low_lim_ind = 0
        for i in range(new_nbins):
            up_lim_ind = np.where(orig_bins <= new_bins[i])[0][-1] + 1
            new_bin = self.wind_sector_dist.iloc[low_lim_ind:up_lim_ind]
            new_dist[i, :] = np.sum(new_bin, 0)
            low_lim_ind = up_lim_ind
        self.reformed_wind_sector_dist = pd.DataFrame(
            new_dist, index=new_bins, columns=self.wind_sector_dist.columns)


if __name__ == '__main__':
    tabfile = 'test.tab'
    tab_reader = MetTABReader(tabfile)
    tab_reader.strip_bins()
    tab_reader.reform_sector_dist(4)
    tab_reader.plot_bar()
